<?php

namespace Modules\DateCalculator\Contracts;

interface WorkingDayInterface
{
    public function convertHourToWorkingDay(int $hour);

    public function isDateBetweenWorkingTime(string $day, int $time, int $minute);
}
