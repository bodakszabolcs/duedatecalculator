<?php

namespace Modules\DateCalculator\Contracts;

interface DueDateCalculatorInterface
{
    public function calculateDueDate($submit, $turnaround);
}
