<?php

namespace Modules\DateCalculator\Entities;

use Modules\DateCalculator\Contracts\DueDateCalculatorInterface;

abstract class BaseDate implements DueDateCalculatorInterface
{
    protected $format = 'Y-m-d H:i';
    protected $notValidDate = "Date is invalid";
    protected $notWorkingDay = "Not working day";

    public function calculateDueDate($submit, $turnaround)
    {
        if(!$this->validateDate($submit)){
            return $this->notValidDate;
        }
        $submit = strtotime($submit);
        $day = new Day();
        if(!$day->isDateBetweenWorkingTime(date('l',$submit),date('H',$submit),date('i'))){
            return $this->notWorkingDay;
        }

        $timeString = $day->convertHourToWorkingDay($turnaround);
        $responseDate = date('Y-m-d H:i',$submit);
        $responseDate = date('Y-m-d H:i',strtotime($responseDate.' +'.$timeString));


        return $day->getNextWorkingDay($responseDate);


    }
    function validateDate($date )
    {
        $d = \DateTime::createFromFormat($this->format, $date);
        return $d && $d->format($this->format) == $date;
    }

}


