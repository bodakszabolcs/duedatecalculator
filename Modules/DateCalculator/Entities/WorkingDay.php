<?php

namespace Modules\DateCalculator\Entities;

use Modules\DateCalculator\Contracts\WorkingDayInterface;

abstract class WorkingDay implements WorkingDayInterface
{
    protected $workingTimeStartHour = 9;
    protected $workingTimeStartMinute = 0;
    protected $workingTimeEndHour = 17;
    protected $workingTimeEndMinute = 0;
    protected $workingDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
    protected $dayNotFound = "Day not found";

    public function convertHourToWorkingDay(int $hour)
    {
        $workingDayLength = $this->workingTimeEndHour - $this->workingTimeStartHour;
        $day = (int)($hour / $workingDayLength);
        $remainder = $hour % $workingDayLength;
        if ($day) {
            return $day . ' day ' . $remainder . ' hour';
        }
        return $remainder . ' hour';
    }

    public function isDateBetweenWorkingTime(string $day, int $time = 0, int $minute = 0)
    {
        if (!in_array($day, $this->workingDays)) {
            return false;
        }
        $timeConverted = new Hour($time, $minute);
        $startConverted = new Hour($this->workingTimeStartHour, $this->workingTimeStartHour);
        $endConverted = new Hour($this->workingTimeEndHour, $this->workingTimeEndMinute);
        if ($timeConverted->getHour() < $startConverted->getHour() || $timeConverted->getHour() > $endConverted->getHour()) {
            return false;
        }
        if ($timeConverted->getHour() === $endConverted->getHour() && $timeConverted->getMinute() > $endConverted->getMinute()) {
            return false;
        }
        return true;
    }

    public function getNextWorkingDay($date, $dayCounter = 0)
    {

        $hour = date('H',strtotime($date));
        if($hour >= $this->workingTimeEndHour){
            $diffHours = $hour - $this->workingTimeEndHour;
            $date =date('Y-m-d '.($this->workingTimeStartHour+$diffHours) . ':i', strtotime($date . " +1 day"));
        }
        $dayString = date('l', strtotime($date));
        if ($dayCounter > 6) {
            return $this->dayNotFound;
        }
        if (in_array($dayString, $this->workingDays)) {

            return $date;
        } else {
            $dayCounter++;
            return $this->getNextWorkingDay(date('Y-m-d H:i', strtotime($date . " +1 day")), $dayCounter);
        }

    }
}
