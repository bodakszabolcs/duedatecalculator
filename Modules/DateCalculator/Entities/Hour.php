<?php

namespace Modules\DateCalculator\Entities;

class Hour
{
    private $hour;
    private $minute;
    private $delininer = ":";

    public function __construct(int $hour, int $minute = 0)
    {
        $this->hour = $hour;
        $this->minute = $minute;
    }

    public function __toString()
    {
        return str_pad($this->hour, 2, "0", STR_PAD_LEFT)
            . $this->delininer
            . str_pad($this->minute, 2, "0", STR_PAD_LEFT);
    }

    /**
     * @return int
     */
    public function getHour(): int
    {
        return $this->hour;
    }

    /**
     * @return int
     */
    public function getMinute(): int
    {
        return $this->minute;
    }
}
