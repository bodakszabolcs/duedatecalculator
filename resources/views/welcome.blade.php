<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">


            <div class="content">
                <div class="title m-b-md">
                    Laravel date Calculator
                </div>
                <form method="get">
                    <label for="submit">submit date/time</label>
                    <input type="text" name="submit" id = "submit" placeholder="2019-11-16 14:50" value="{{\Illuminate\Support\Arr::get($_REQUEST,'submit')}}">
                    <label for="turnaround">turnaround time</label>
                    <input type="text" name="turnaround" id = "turnaround" value="{{\Illuminate\Support\Arr::get($_REQUEST,'turnaround')}}">
                    <button type="submit">Calculate</button>
                    @if(isset($_REQUEST['submit']) &&!empty($_REQUEST['submit']) && isset($_REQUEST['turnaround']) && !empty($_REQUEST['turnaround']))
                        @php $dateCalculator = new \Modules\DateCalculator\Entities\DateCalculator();
                               dump($dateCalculator->calculateDueDate($_REQUEST['submit'],$_REQUEST['turnaround']))
                        @endphp
                    @endif
                </form>
                <?php



                ?>

            </div>
        </div>
    </body>
</html>
