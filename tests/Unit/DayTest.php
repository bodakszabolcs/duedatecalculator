<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\DateCalculator\Entities\Day;
use Tests\TestCase;

class DayTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testconvertHourToWorkingDay1()
    {
        $day = new Day();

        $this->assertTrue($day->convertHourToWorkingDay(8) == '1 day 0 hour',$day->convertHourToWorkingDay(8));
    }
    public function testconvertHourToWorkingDay2()
    {
        $day = new Day();

        $this->assertTrue($day->convertHourToWorkingDay(11) == '1 day 3 hour',$day->convertHourToWorkingDay(11));
    }
    public function testconvertHourToWorkingDay3()
    {
        $day = new Day();

        $this->assertTrue($day->convertHourToWorkingDay(5) == '5 hour',$day->convertHourToWorkingDay(5));
    }
    public function testisDateBetweenWorkingTime1()
    {
        $day = new Day();

        $this->assertTrue(!$day->isDateBetweenWorkingTime('Sunday'));
    }
    public function testisDateBetweenWorkingTime2()
    {
        $day = new Day();

        $this->assertTrue(!$day->isDateBetweenWorkingTime('Monday',17,1));
    }
    public function testisDateBetweenWorkingTime3()
    {
        $day = new Day();

        $this->assertTrue($day->isDateBetweenWorkingTime('Monday',15,1));
    }
}
