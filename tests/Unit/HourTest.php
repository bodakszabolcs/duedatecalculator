<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\DateCalculator\Entities\Hour;
use Tests\TestCase;

class HourTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testFormat1()
    {
        $hour = new Hour(2,2);
        $this->assertTrue($hour."" === '02:02');
    }
    public function testFormat2()
    {
        $hour = new Hour(12);
        $this->assertTrue($hour == '12:00');

    }
    public function testFormat3()
    {
        $hour = new Hour(0,1);
        $this->assertTrue($hour == '00:01');
    }

}
