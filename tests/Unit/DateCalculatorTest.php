<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\DateCalculator\Entities\DateCalculator;
use Tests\TestCase;

class DateCalculatorTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testInvalidDate()
    {
        $dateCalculator = new DateCalculator();
        $this->assertTrue($dateCalculator->calculateDueDate('2019-02-30 12:10',8) == "Date is invalid");
    }
    public function testNotWorkingDay(){
        $dateCalculator = new DateCalculator();
        $this->assertTrue($dateCalculator->calculateDueDate('2019-11-16 12:10',8) == "Not working day");
    }
    public function testNotWorkingDayWeekday(){
        $dateCalculator = new DateCalculator();

        $this->assertTrue($dateCalculator->calculateDueDate('2019-11-14 12:10',16) == "2019-11-18 12:10");

    }
    public function testNotWorkingDayNotWeekday(){
        $dateCalculator = new DateCalculator();

        $this->assertTrue($dateCalculator->calculateDueDate('2019-11-11 12:10',5) == "2019-11-12 9:10");

    }
}
